#![allow(dead_code)]
#![allow(non_upper_case_globals)]
#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(unused_variables)]
#![allow(unused_imports)]

extern crate ascii;
extern crate i2cdev;
extern crate pretty_env_logger;
extern crate spidev;
extern crate structopt;

mod cmdline;
use cmdline::Opt;
use std::cell::RefCell;

#[macro_use]
extern crate log;

use std::path::PathBuf;
use structopt::StructOpt;

use rumqtt::{MqttClient, MqttOptions, QoS};
use std::{thread, time::Duration};

use i2cdev::core::{I2CMessage, I2CTransfer};
use i2cdev::linux::{LinuxI2CBus, LinuxI2CMessage};

use ascii::AsciiChar;

use spidev::{SpiModeFlags, Spidev, SpidevOptions, SpidevTransfer};
use std::io;
use std::io::prelude::*;

// MQTT basics
const MQTT_SERVER_IP: &str = "192.168.0.185";
const MQTT_SERVER_PORT: u16 = 1883;
const MQTT_CLIENT_ID: &str = "abiyo-sensor-conductivity";
const MQTT_TOPIC_CONDUCTIVITY: &str = "abiyo/sensor/water/conductivity";
const MQTT_TOPIC_TEMPERATURE: &str = "abiyo/sensor/water/temperature";
const MQTT_TOPIC_PH: &str = "abiyo/sensor/water/ph";

const PUBLISHING_HYSTERESIS_TEMPERATUR: f64 = 0.025;
const PUBLISHING_HYSTERESIS_CONDUCTIVITY: f64 = 0.005;

pub mod cn0349 {

    use i2cdev::core::{I2CMessage, I2CTransfer};
    use i2cdev::linux::{LinuxI2CBus, LinuxI2CMessage};

                use std::{thread, time};
    #[derive(Clone, Copy)]
    #[repr(u8)]
    enum AD5934Regs {
        Control16 = 0x80,
        StartFrq24 = 0x82,
        FrqInc24 = 0x85,
        NumInc16 = 0x88,
        SettleTime16 = 0x8A,
        Status = 0x8F,
        Real16 = 0x94,
        Imaginary16 = 0x96,
    }

    #[derive(Clone, Copy)]
    #[repr(u16)]
    enum AD5934ControlRegMode {
        Nop1 = 0x0,
        InitStartFrq = 0x1,
        StartSweep = 0x2,
        IncFrq = 0x3,
        RepFrq = 0x4,
        Nop2 = 0x8,
        Nop3 = 0x9,
        Powerdown = 0xA,
        Standby = 0xB,
        Nop4 = 0xC,
        Nop5 = 0xD,
    }
    const AD5934ControlRegModeOffset: u8 = 12;

    #[derive(Clone, Copy)]
    #[repr(u16)]
    enum AD5934ControlRegRange {
        V2 = 0x0,
        MV200 = 0x1,
        MV400 = 0x2,
        V1 = 0x3,
    }
    const AD5934ControlRegRangeOffset: u8 = 9;

    #[derive(Clone, Copy)]
    #[repr(u16)]
    enum AD5934ControlRegPGAGain {
        X1 = 0x1,
        X5 = 0x0,
    }
    const AD5934ControlRegPGAGainOffset: u8 = 8;

    #[derive(Clone, Copy)]
    #[repr(u8)]
    enum AD5934StatusRegMasks {
        DataValid = 0x2,
        SweepComplete = 0x4,
    }

    #[derive(Clone, Copy)]
    #[repr(u8)]
    enum ADG715SwitchRfb {
        Rfb100 = 0x1,
        Rfb1000 = 0x2,
    }

    #[derive(Clone, Copy)]
    #[repr(u8)]
    enum ADG715SwitchSrc {
        Y = 0x80,
        Rtd = 0x40,
        R10000 = 0x20,
        R1000 = 0x10,
        R100 = 0x08,
    }

    ////////////////////////////

    pub struct CN0349 {
        bus: std::cell::RefCell<LinuxI2CBus>,
    }

    impl CN0349 {
        const AD5934_I2C_ADDR: u16 = 0x0D;
        const ADG715_I2C_ADDR: u16 = 0x48;

        pub fn new(i2cdev: std::cell::RefCell<LinuxI2CBus>) -> Self {
            Self { bus: i2cdev }
        }

        /////////////////////////////////////////////////////////

        fn ADG715_write_val8(&self, value: u8) -> Result<(), i2cdev::linux::LinuxI2CError> {
            let payload: [u8; 1] = [value];

            let mut msgs: [LinuxI2CMessage; 1] =
                [LinuxI2CMessage::write(&payload).with_address(CN0349::ADG715_I2C_ADDR)];

            self.bus.borrow_mut().transfer(&mut msgs)?;
            return Ok(());
        }

        fn ADG715_read_val8(&self, value: u8) -> Result<u8, i2cdev::linux::LinuxI2CError> {
            let mut payload: [u8; 1] = [0; 1];

            let mut msgs: [LinuxI2CMessage; 1] =
                [LinuxI2CMessage::read(&mut payload).with_address(CN0349::ADG715_I2C_ADDR)];

            self.bus.borrow_mut().transfer(&mut msgs)?;
            return Ok(payload[0]);
        }

        //////////////////////////////////////////////////////

        fn set_switch(
            &self,
            source: ADG715SwitchSrc,
            rfb: ADG715SwitchRfb,
        ) -> Result<(), i2cdev::linux::LinuxI2CError> {
            self.ADG715_write_val8((source as u8) | (rfb as u8))?;

            return Ok(());
        }

        ////////////////////////////////////////////////

        fn AD5934_read_reg8_val8(&self, reg: u8) -> Result<u8, i2cdev::linux::LinuxI2CError> {
            let reg_command: [u8; 2] = [0b10110000, reg];
            let block_read: [u8; 2] = [0b10100001, 1];
            let mut data: [u8; 1] = [0; 1];

            let mut msgs1: [LinuxI2CMessage; 2] = [
                LinuxI2CMessage::write(&reg_command).with_address(CN0349::AD5934_I2C_ADDR),
                LinuxI2CMessage::write(&block_read).with_address(CN0349::AD5934_I2C_ADDR),
            ];

            self.bus.borrow_mut().transfer(&mut msgs1).unwrap();

            let mut msgs: [LinuxI2CMessage; 1] = [
                LinuxI2CMessage::read(&mut data).with_address(CN0349::AD5934_I2C_ADDR),
            ];

            self.bus.borrow_mut().transfer(&mut msgs).unwrap();

            return Ok(data[0]);
        }

        fn AD5934_read_reg8_val16(&self, reg: u8) -> Result<u16, i2cdev::linux::LinuxI2CError> {
            let value: u16;
            let reg_command: [u8; 2] = [0b10110000, reg];
            let block_read: [u8; 2] = [0b10100001, 2];
            let mut data: [u8; 2] = [0; 2];

            let mut msgs: [LinuxI2CMessage; 2] = [
                LinuxI2CMessage::write(&reg_command).with_address(CN0349::AD5934_I2C_ADDR),
                LinuxI2CMessage::write(&block_read).with_address(CN0349::AD5934_I2C_ADDR),
            ];

            self.bus.borrow_mut().transfer(&mut msgs)?;

            let mut msgs_r: [LinuxI2CMessage; 1] = [
                LinuxI2CMessage::read(&mut data).with_address(CN0349::AD5934_I2C_ADDR),
            ];

            self.bus.borrow_mut().transfer(&mut msgs_r)?;

            value = ((data[0] as u16) << 8) | data[1] as u16;
            return Ok(value);
        }

        fn AD5934_read_reg8_val24(&self, reg: u8) -> Result<u32, i2cdev::linux::LinuxI2CError> {
            let value: u32;
            let reg_command: [u8; 2] = [0b10110000, reg];
            let block_read: [u8; 2] = [0b10100001, 3];
            let mut data: [u8; 3] = [0; 3];

            let mut msgs: [LinuxI2CMessage; 3] = [
                LinuxI2CMessage::write(&reg_command).with_address(CN0349::AD5934_I2C_ADDR),
                LinuxI2CMessage::write(&block_read).with_address(CN0349::AD5934_I2C_ADDR),
                LinuxI2CMessage::read(&mut data).with_address(CN0349::AD5934_I2C_ADDR),
            ];

            self.bus.borrow_mut().transfer(&mut msgs)?;

            let mut msgs_r: [LinuxI2CMessage; 1] = [
                LinuxI2CMessage::read(&mut data).with_address(CN0349::AD5934_I2C_ADDR),
            ];

            self.bus.borrow_mut().transfer(&mut msgs_r)?;

            value = ((data[0] as u32) << 16) | ((data[1] as u32) << 8) | data[2] as u32;
            return Ok(value);
        }

        fn AD5934_write_reg8_val8(
            &self,
            reg: u8,
            val: u8,
        ) -> Result<(), i2cdev::linux::LinuxI2CError> {
            let reg_command: [u8; 2] = [0b10110000, reg];
            let block_write: [u8; 3] = [0b10100000, 1, val];

            let mut msgs: [LinuxI2CMessage; 2] = [
                LinuxI2CMessage::write(&reg_command).with_address(CN0349::AD5934_I2C_ADDR),
                LinuxI2CMessage::write(&block_write).with_address(CN0349::AD5934_I2C_ADDR),
            ];

            self.bus.borrow_mut().transfer(&mut msgs)?;

            return Ok(());
        }

        fn AD5934_write_reg8_val16(
            &self,
            reg: u8,
            val: u16,
        ) -> Result<(), i2cdev::linux::LinuxI2CError> {
            let reg_command: [u8; 2] = [0b10110000, reg];

            let block_write: [u8; 4] = [
                0b10100000,
                2,
                ((val >> 8) & 0xff) as u8,
                ((val >> 0) & 0xff) as u8,
            ];

            let mut msgs: [LinuxI2CMessage; 2] = [
                LinuxI2CMessage::write(&reg_command).with_address(CN0349::AD5934_I2C_ADDR),
                LinuxI2CMessage::write(&block_write).with_address(CN0349::AD5934_I2C_ADDR),
            ];

            self.bus.borrow_mut().transfer(&mut msgs)?;

            return Ok(());
        }

        fn AD5934_write_reg8_val24(
            &self,
            reg: u8,
            val: u32,
        ) -> Result<(), i2cdev::linux::LinuxI2CError> {
            let reg_command: [u8; 2] = [0b10110000, reg];

            let block_write: [u8; 5] = [
                0b10100000,
                3,
                ((val >> 16) & 0xff) as u8,
                ((val >> 8) & 0xff) as u8,
                ((val >> 0) & 0xff) as u8,
            ];

            let mut msgs: [LinuxI2CMessage; 2] = [
                LinuxI2CMessage::write(&reg_command).with_address(CN0349::AD5934_I2C_ADDR),
                LinuxI2CMessage::write(&block_write).with_address(CN0349::AD5934_I2C_ADDR),
            ];

            self.bus.borrow_mut().transfer(&mut msgs)?;

            return Ok(());
        }

        ////////////////////////////////////////////////////////

        fn set_start_frq(&self, frq: u32) -> Result<(), i2cdev::linux::LinuxI2CError> {
            return self.AD5934_write_reg8_val24(AD5934Regs::StartFrq24 as u8, frq);
        }

        fn set_frq_inc(&self, inc: u32) -> Result<(), i2cdev::linux::LinuxI2CError> {
            return self.AD5934_write_reg8_val24(AD5934Regs::FrqInc24 as u8, inc);
        }

        fn set_num_inc(&self, num: u16) -> Result<(), i2cdev::linux::LinuxI2CError> {
            return self.AD5934_write_reg8_val16(AD5934Regs::NumInc16 as u8, num);
        }

        fn set_settling_time(&self, time: u16) -> Result<(), i2cdev::linux::LinuxI2CError> {
            return self.AD5934_write_reg8_val16(AD5934Regs::SettleTime16 as u8, time & 0x1ff);
        }

        fn set_control(
            &self,
            mode: AD5934ControlRegMode,
            range: AD5934ControlRegRange,
            gain: AD5934ControlRegPGAGain,
        ) -> Result<(), i2cdev::linux::LinuxI2CError> {
            // Assume internal clock

            let val: u16 = ((mode as u16) << AD5934ControlRegModeOffset)
                | ((range as u16) << AD5934ControlRegRangeOffset)
                | ((gain as u16) << AD5934ControlRegPGAGainOffset);
            return self.AD5934_write_reg8_val16(AD5934Regs::Control16 as u8, val);
        }

        fn get_status_data_is_valid(&self) -> Result<bool, i2cdev::linux::LinuxI2CError> {
            let status: u8 = self.AD5934_read_reg8_val8(AD5934Regs::Status as u8)?;

            if (status & (AD5934StatusRegMasks::DataValid as u8)) != 0 {
                return Ok(true);
            }

            return Ok(false);
        }

        fn get_status_sweep_is_complete(&self) -> Result<bool, i2cdev::linux::LinuxI2CError> {
            let status: u8 = self.AD5934_read_reg8_val8(AD5934Regs::Status as u8)?;

            if (status & (AD5934StatusRegMasks::SweepComplete as u8)) != 0 {
                return Ok(true);
            }

            return Ok(false);
        }

        fn get_real_data(&self) -> Result<u16, i2cdev::linux::LinuxI2CError> {
            let value: u16 = self.AD5934_read_reg8_val16(AD5934Regs::Real16 as u8)?;
            return Ok(value);
        }

        fn get_imaginary_data(&self) -> Result<u16, i2cdev::linux::LinuxI2CError> {
            let value: u16 = self.AD5934_read_reg8_val16(AD5934Regs::Imaginary16 as u8)?;
            return Ok(value);
        }

        fn read_mag(&self) -> Result<f64, i2cdev::linux::LinuxI2CError> {
            let ru = self.get_real_data()?;
            let iu = self.get_imaginary_data()?;

            let r: i16 = ru as i16;
            let i: i16 = iu as i16;

            return Ok(((r as f64) * (r as f64) + (i as f64) * (i as f64)).sqrt());
        }
        //////////////////////////////////////////////////

        fn calib(&self, range : AD5934ControlRegRange, gain: AD5934ControlRegPGAGain) -> Result<(f64, f64), i2cdev::linux::LinuxI2CError> {
            let rfb = ADG715SwitchRfb::Rfb1000;

            // Calibrate 10K Resistor

            self.set_switch(ADG715SwitchSrc::R10000, rfb)?;
            let yl = 0.0001;

            self.set_control(AD5934ControlRegMode::Standby, range, gain)?;
            self.set_control(AD5934ControlRegMode::InitStartFrq, range, gain)?;
            self.set_control(AD5934ControlRegMode::StartSweep, range, gain)?;

            loop {
                if self.get_status_data_is_valid()? {
                    break;
                }
            }
            let nl = self.read_mag()?;

            // Calibrate 1K Resistor

            self.set_switch(ADG715SwitchSrc::R1000, rfb)?;
            let yh = 0.001;

            self.set_control(AD5934ControlRegMode::Standby, range, gain)?;
            self.set_control(AD5934ControlRegMode::InitStartFrq, range, gain)?;
            self.set_control(AD5934ControlRegMode::StartSweep, range, gain)?;

            loop {
                if self.get_status_data_is_valid()? {
                    break;
                }
            }
            let nh = self.read_mag()?;

            // Calculate calibration data

            let gf = ((yh - yl)) / (nh - nl);
            let nos = nh - (yh / gf);

//            println!("low {}, high {}, nos {}, gf {}", nl, nh, nos, gf);

            return Ok((gf, nos));
        }

        fn measure(&self, range : AD5934ControlRegRange, gain: AD5934ControlRegPGAGain) -> Result<f64, i2cdev::linux::LinuxI2CError> {

            // Assumes that the ADG715Switch is set correctly, Source and feedback resistor

            self.set_control(AD5934ControlRegMode::Standby, range, gain)?;
            self.set_control(AD5934ControlRegMode::InitStartFrq, range, gain)?;
            self.set_control(AD5934ControlRegMode::StartSweep, range, gain)?;

            loop {
                if self.get_status_data_is_valid()? {
                    break;
                }
            }

            return Ok(self.read_mag()?);
        }

        ////////////////////////////////////////////////

        pub fn measure_temperature(&self) -> Result<f64, i2cdev::linux::LinuxI2CError> {
            self.set_start_frq(0x418937)?;
            self.set_frq_inc(0x418937 / 16)?;
            self.set_num_inc(1)?;
            self.set_settling_time(1)?;

            let range = AD5934ControlRegRange::V2;
            let gain = AD5934ControlRegPGAGain::X5;

            let (gf, nos) = self.calib(range, gain)?;
            let mut acc = 0.0;
            let avg = 10;

//            println!("nos: {:04} gf: {:04}", nos, gf);
            self.set_switch(ADG715SwitchSrc::Rtd, ADG715SwitchRfb::Rfb1000)?;

            for _i in 0..avg {
                let val = self.measure(range, gain)?;
//                println!("raw: {:04} ", val);
                acc = acc + (1.0 / ((val - nos) * gf));
            }

            let temp = acc / avg as f64;
            println!("Ohm: {:01}", temp);
            return Ok(-24.0 * (temp + 312.9138212).ln() + 213.3720658);
        }

        pub fn measure_conductivity(&self) -> Result<f64, i2cdev::linux::LinuxI2CError> {
            self.set_start_frq(0x418937)?;
            self.set_frq_inc(0x418937 / 16)?;
            self.set_num_inc(1)?;
            self.set_settling_time(16)?;

            let range = AD5934ControlRegRange::MV200;
            let gain = AD5934ControlRegPGAGain::X5;

            let (gf, nos) = self.calib(range, gain)?;
            let mut acc = 0.0;
            let avg = 10;

            self.set_switch(ADG715SwitchSrc::Y, ADG715SwitchRfb::Rfb1000)?;

            for _i in 0..avg {
                let val = self.measure(range, gain)?;
//                println!("raw: {:04} ", val);
                acc = acc + ((val - nos) * gf * 1000000.0) / 1000.0;
            }

            let temp = acc / avg as f64;

            return Ok(temp);
        }
    }
}

pub mod cn0326 {

    use std::io;
    use spidev::{SpiModeFlags, Spidev, SpidevOptions, SpidevTransfer};

    #[derive(Clone, Copy)]
    #[repr(u8)]
    enum AD7793Regs {
        //Control16 = 0x80,
        STATUS_R = 0x0,
        MODE_16 = 0x1,
        CONF_16 = 0x2,
        DATA_24 = 0x3,
        ID = 0x4,
        IO = 0x5,
        OFFSET_24 = 0x6,
        FULL_24 = 0x7,
    }


    const SPI_BUS: &str = "/dev/spidev0.1";

    pub enum Status {
        Channel0 = 0x01,
        Channel1 = 0x02,
        Channel2 = 0x04,
        Error = 0x40,
        Ready = 0x80,
    }

    pub enum Mode {
        Continuous = 0x0,
        Single = 0x1,
        Idle = 0x2,
        Powerdown = 0x3,
        InternalZero = 0x4,
        InternalFull = 0x5,
        SystemZero = 0x6,
        SystemFull = 0x7,
    }

    enum Mode2 {
        UpdateRate = 0xf, // 500ms conv cycle
        ClockRef = 0x0,   // Internal clock and output on CLK pin
    }

    pub enum Reg {
        Comm8,
        Status8,
        Mode16,
        Conf16,
        Data24,
        Id8,
        Io8,
        Offset24,
        Full24,
    }

    const OPCODE_REG_MASK: u8 = 0x7; // Reg id must be >= 0
    const OPCODE_REG_OFFSET: isize = 3;
    const COMM_RNW_BIT_MASK: u8 = 0x40;

    const HALF_DUPLEX_DUMMY_BYTE: u8 = 0x00; // Bytes that are put into spi write buffers for half duplex devices

    const CHIP_ID_LOW_NIBBLE: u8 = 0x0b; // The ID that is return by the ID_8 register, only observe the lower nibble

    impl Into<u8> for Reg {
        fn into(self) -> u8 {
            match self {
                Reg::Comm8 => 0x88 << OPCODE_REG_OFFSET,
                Reg::Status8 => 0x0 << OPCODE_REG_OFFSET,
                Reg::Mode16 => 0x1 << OPCODE_REG_OFFSET,
                Reg::Conf16 => 0x2 << OPCODE_REG_OFFSET,
                Reg::Data24 => 0x3 << OPCODE_REG_OFFSET,
                Reg::Id8 => 0x4 << OPCODE_REG_OFFSET,
                Reg::Io8 => 0x5 << OPCODE_REG_OFFSET,
                Reg::Offset24 => 0x6 << OPCODE_REG_OFFSET,
                Reg::Full24 => 0x7 << OPCODE_REG_OFFSET,
            }
        }
    }
    ////////////////////////

    use std::cell::RefCell;

    pub struct CN0326 {
        dev: RefCell<Spidev>,
    }

    impl CN0326 {
        //    const SPIBUS : &'static str = "/dev/spidev0.0";

        pub fn new(spi: RefCell<Spidev>) -> Self {
            Self { dev: spi }
        }

        /////////////////////////////
        /*
            /// Perform full duplex operations using Ioctl
            fn full_duplex(spi: &mut Spidev) -> io::Result<()> {
                // "write" transfers are also reads at the same time with
                // the read having the same length as the write
                let mut transfer = SpidevTransfer::write(&[0x01, 0x02, 0x03]);
                spi.transfer(&mut transfer)?;
                println!("{:?}", transfer.rx_buf);
                Ok(())
            }
        */
        fn read_reg(&self, reg: AD7793Regs) -> Result<u8, io::Error> {
            let mut register_command: [u8; 2] = [0; 2];
            let mut rx_buf: [u8; 2] = [0; 2];

            register_command[0] = 0x00 | 0x40 | ((reg as u8) << 3) | 0x00 | 0x00;
            println!("Sending command {:#04x}", register_command[0]);
            let mut transfer = SpidevTransfer::read_write(&register_command, &mut rx_buf);
            transfer.cs_change = 0;
            transfer.delay_usecs = 0;

            self.dev
                .borrow_mut()
                .transfer(&mut transfer)?;
            println!("{:?}", rx_buf);

            return Ok(rx_buf[1]);
        }
        ///  
        fn read_reg8_val8(&self, reg: Reg) -> u8 {
            const WR_LEN: usize = 1;
            const RD_LEN: usize = 1;
            const WR_OFFSET: usize = 0;
            const RD_OFFSET: usize = WR_LEN;
            const LEN: usize = WR_LEN + RD_LEN;

            let mut write_buf: [u8; LEN] = [HALF_DUPLEX_DUMMY_BYTE; LEN];
            let mut read_buf: [u8; LEN] = [0x0u8; LEN];

            write_buf[WR_OFFSET] = Into::<u8>::into(reg) | COMM_RNW_BIT_MASK;

            let mut transfer = SpidevTransfer::read_write(&write_buf, &mut read_buf);
            self.dev
                .borrow_mut()
                .transfer(&mut transfer)
                .map_err(|e| format!("SPI transaction failed: {}", e))
                .expect("Unable to transfer");

            return (read_buf[RD_OFFSET + 0] as u8) << 0;
        }

        fn write_reg8_val8(&self, reg: Reg, val: u8) {
            const WR_LEN: usize = 2;
            const LEN: usize = WR_LEN;

            let write_buf: [u8; LEN] = [reg.into(), ((val >> 0) & 0xff) as u8];
            let mut read_buf: [u8; LEN] = [0x0u8; LEN];

            let mut transfer = SpidevTransfer::read_write(&write_buf, &mut read_buf);
            self.dev
                .borrow_mut()
                .transfer(&mut transfer)
                .map_err(|e| format!("SPI transaction failed: {}", e))
                .expect("Unable to transfer");
        }

        fn read_reg8_val16(&self, reg: Reg) -> u16 {
            const WR_LEN: usize = 1;
            const RD_LEN: usize = 2;
            const WR_OFFSET: usize = 0;
            const RD_OFFSET: usize = WR_LEN;
            const LEN: usize = WR_LEN + RD_LEN;

            let mut write_buf: [u8; LEN] = [HALF_DUPLEX_DUMMY_BYTE; LEN];
            let mut read_buf: [u8; LEN] = [0x0u8; LEN];

            write_buf[WR_OFFSET] = Into::<u8>::into(reg) | COMM_RNW_BIT_MASK;

            let mut transfer = SpidevTransfer::read_write(&write_buf, &mut read_buf);
            self.dev
                .borrow_mut()
                .transfer(&mut transfer)
                .map_err(|e| format!("SPI transaction failed: {}", e))
                .expect("Unable to transfer");

            return ((read_buf[RD_OFFSET + 0] as u16) << 8)
                | ((read_buf[RD_OFFSET + 1] as u16) << 0);
        }

        fn write_reg8_val16(&self, reg: Reg, val: u16) {
            const WR_LEN: usize = 3;
            const WR_OFFSET: usize = 0;
            const LEN: usize = WR_LEN;

            let write_buf: [u8; LEN] = [
                reg.into(),
                ((val >> 8) & 0xff) as u8,
                ((val >> 0) & 0xff) as u8,
            ];
            let mut read_buf: [u8; LEN] = [0x0u8; LEN];

            let mut transfer = SpidevTransfer::read_write(&write_buf, &mut read_buf);
            self.dev
                .borrow_mut()
                .transfer(&mut transfer)
                .map_err(|e| format!("SPI transaction failed: {}", e))
                .expect("Unable to transfer");
        }

        fn read_reg8_val24(&self, reg: Reg) -> u32 {
            const WR_LEN: usize = 1;
            const RD_LEN: usize = 3;
            const WR_OFFSET: usize = 0;
            const RD_OFFSET: usize = WR_LEN;
            const LEN: usize = WR_LEN + RD_LEN;

            let mut write_buf: [u8; LEN] = [HALF_DUPLEX_DUMMY_BYTE; LEN];
            let mut read_buf: [u8; LEN] = [0x0u8; LEN];

            write_buf[WR_OFFSET] = Into::<u8>::into(reg) | COMM_RNW_BIT_MASK;

            let mut transfer = SpidevTransfer::read_write(&write_buf, &mut read_buf);
            self.dev
                .borrow_mut()
                .transfer(&mut transfer)
                .map_err(|e| format!("SPI transaction failed: {}", e))
                .expect("Unable to transfer");

            return ((read_buf[RD_OFFSET + 0] as u32) << 16)
                | ((read_buf[RD_OFFSET + 1] as u32) << 8)
                | ((read_buf[RD_OFFSET + 2] as u32) << 0);
        }

        fn write_reg8_val24(&self, reg: Reg, val: u32) {
            const WR_LEN: usize = 4;
            const WR_OFFSET: usize = 0;
            const LEN: usize = WR_LEN;

            let write_buf: [u8; LEN] = [
                reg.into(),
                ((val >> 16) & 0xff) as u8,
                ((val >> 8) & 0xff) as u8,
                ((val >> 0) & 0xff) as u8,
            ];
            let mut read_buf: [u8; LEN] = [0x0u8; LEN];

            let mut transfer = SpidevTransfer::read_write(&write_buf, &mut read_buf);
            self.dev
                .borrow_mut()
                .transfer(&mut transfer)
                .map_err(|e| format!("SPI transaction failed: {}", e))
                .expect("Unable to transfer");
        }

        /////////////////////////////

        fn reset_device(&self) {
            let write_buf: [u8; 4] = [0xff, 0xff, 0xff, 0xff];
            let mut read_buf: [u8; 4] = [0x0u8; 4];

            let mut transfer = SpidevTransfer::read_write(&write_buf, &mut read_buf);
            self.dev
                .borrow_mut()
                .transfer(&mut transfer)
                .map_err(|e| format!("SPI transaction failed: {}", e))
                .expect("Unable to transfer");

            assert_eq!(self.read_reg8_val8(Reg::Id8) & 0xf, CHIP_ID_LOW_NIBBLE);
            // ID must be 0xXb;
        }

        fn set_io(&self) {
            const REG: Reg = Reg::Io8;

            // Bitffield content
            const IEXCDIR: u8 = 0x0;
            const IEXCEN: u8 = 0x2; // 210uA current source hits a 5k/5k voltage divider biasing the pH probe

            let v = (IEXCDIR << 2) | (IEXCEN << 0);

            self.write_reg8_val8(REG, v);
            assert_eq!(self.read_reg8_val8(REG), v);
        }

        fn set_conf(&self, ch: u16) {
            const REG: Reg = Reg::Conf16;

            // Bitffield content
            const BUFFER_INPUT: u16 = 0; // # unbuffered adc input for use with gain 1x
            const REF_SEL: u16 = 0; // # extern reference taken from REFIN1+ (== 2.1V / 2) driven by 210uA current source over 5k/5k voltage divider
            const GAIN: u16 = 0; // # 1x gain
            const BOOST: u16 = 0; // # no boosting of bias since not in use
            const BIAS: u16 = 0; // no bias
            const POLARITY: u16 = 0; // # Bipolar operation

            let v: u16 = ch
                | (BUFFER_INPUT << 4)
                | (REF_SEL << 7)
                | (GAIN << 8)
                | (BOOST << 11)
                | (POLARITY << 12)
                | (BIAS << 14);

            self.write_reg8_val16(REG, v);
            assert_eq!(self.read_reg8_val16(REG), v);
        }

        fn set_mode(&self, mode: Mode) {
            const REG: Reg = Reg::Mode16;

            const UPDATE_RATE: u16 = 0b1111; // f_adc = 4.17hz, settling time 480ms
            const CLOCK_REF: u16 = 0x0; // Use internal 64khz clock and no output on clk pin

            let v = UPDATE_RATE | (CLOCK_REF << 6) | ((mode as u16) << 13);

            self.write_reg8_val16(REG, v);
            assert_eq!(self.read_reg8_val16(REG), v);
        }

        fn wait_data_ready(&self) -> Result<(), ()> {
            loop {
                let status = self.read_reg8_val8(Reg::Status8);

                if (status & (Status::Error as u8)) != 0 {
                    return Err(());
                }
                if (status & (Status::Ready as u8)) == 0 {
                    return Ok(());
                }
            }
        }

        fn read_full_scale(&self) -> u32 {
            self.read_reg8_val24(Reg::Full24)
        }

        fn write_full_scale(&self, val: u32) {
            self.write_reg8_val24(Reg::Full24, val);
        }

        fn read_data(&self) -> u32 {
            self.read_reg8_val24(Reg::Data24)
        }

        pub fn measure_uv(&self) -> f64 {
            self.reset_device();

            self.set_io();
            self.set_conf(0);
            self.set_mode(Mode::Idle);

            std::thread::sleep(std::time::Duration::from_millis(150));

            // Calibrate for Value Zero, result is stored internally in the ADC
            //d.set_conf(3); // For Mode::SystemZero use channel 3 which is AIN1- vs AIN1- (== short)
            self.set_mode(Mode::InternalZero);
            self.wait_data_ready()
                .expect("Error while Internal Zero Calibration");

            // Calibrate for Value Max, result is stored internally in the ADC
            self.set_mode(Mode::InternalFull);
            self.wait_data_ready()
                .expect("Error while Internal Full Calibration");

            // Measure actual pH probe and read Data
            self.set_conf(0); // AIN1+ vs AIN1- is the pH Probe
            self.set_mode(Mode::Single);
            self.wait_data_ready().expect("Error while ADC measurement");

            let raw = self.read_data() as f64;

            let u_volt = (raw - 0x80_0000 as f64) // Convert register content to signed float
                     /
                     (0x100_0000 as f64)
                * 2_100_000.0; // 2.1V resulting from 210uA current through a 5k/5k(==10k) voltage divider going into REFIN1+

            u_volt
        }


    pub fn measure_ph(&self, temperature : f64) -> f64 {

        // TODO describe model and assumptions behind calibration
        let calib : ProbeCalibration = ProbeCalibration::default();

        7.0 - (((self.measure_uv() - calib.calibration_ph7_in_uv) / (calib.nerst_factor + temperature * calib.nerst_deviate)) * calib.calibration_probe_scale)
    }

    }

//    #[builder(default)]
//    #[derive(derive_builder::Builder, Debug, Serialize, Deserialize)]
    pub struct ProbeCalibration {
        name : &'static str,
        // Values that are specific to the type of probe
        nerst_factor : f64,
        nerst_deviate : f64,

        // Values that are specific to the particular probe
        // Place probe into 7.0 buffer and record the uV ADC reading here:
        calibration_ph7_in_uv : f64,
        // After setting the zero offset, place probe into 4.0 buffer and record pH reading with CALIBRATION_PROBE_SCALE set to 1.0
        calibration_probe_scale : f64
    }
/*
    impl Default for  ProbeCalibration {
        fn default() -> ProbeCalibration {
            ProbeCalibration {
                name : "blue",
                // Values that are specific to the type of probe
                nerst_factor : 54.20 * 1_000.0, // uV per pH at 0 Celsius
                nerst_deviate : 0.1984 * 1_000.0, // uV per pH per degree celsius

                // Values that are specific to the particular probe
                // Place probe into 7.0 buffer and record the uV ADC reading here:
                calibration_ph7_in_uv : -46_200.1, // uV ADC reading at pH7.0
                // After setting the zero offset, place probe into 4.0 buffer and record pH reading with CALIBRATION_PROBE_SCALE set to 1.0
                calibration_probe_scale : ((7.00 - 4.00) / (7.00 - 4.8)) // Specific probe scale factor



                // Values that are specific to the particular probe
                // Place probe into 7.0 buffer and record the uV ADC reading here:
                calibration_ph7_in_uv : 150.0, // uV ADC reading at pH7.0
                // After setting the zero offset, place probe into 4.0 buffer and record pH reading with CALIBRATION_PROBE_SCALE set to 1.0
                calibration_probe_scale : (7.00 - 4.00) / (7.00 - 4.79) // Specific probe scale factor
            }
        }
    }
*/
    impl Default for  ProbeCalibration {
        fn default() -> ProbeCalibration {
            ProbeCalibration {
                name : "white",
                // Values that are specific to the type of probe
                nerst_factor : 54.20 * 1_000.0, // uV per pH at 0 Celsius
                nerst_deviate : 0.1984 * 1_000.0, // uV per pH per degree celsius


                // Values that are specific to the particular probe
                // Place probe into 7.0 buffer and record the uV ADC reading here:
                calibration_ph7_in_uv : -46_200.1, // uV ADC reading at pH7.0
                // After setting the zero offset, place probe into 4.0 buffer and record pH reading with CALIBRATION_PROBE_SCALE set to 1.0
                calibration_probe_scale : ((7.00 - 4.00) / (7.00 - 4.8)) // Specific probe scale factor
            }
        }
    }

}






// Temperatur an der der EC Skalierungfaktor bestimmt wurde in Grad
const ecF1: f64 = 20.0;

// Temperaturkoeffiziert der Loesung in % pro Grad
// ((Unkalibrierten EC Wert bei 30 Grad) - (Unkalibrierten EC Wert bei 20 Grad))
const ecF2: f64 = 0.0204;

// Skalierungsfaktor
const ecF3: f64 = 2.15;

use cn0349::CN0349;
use cn0326::CN0326;

fn main() {
    pretty_env_logger::init();
    let opt = Opt::from_args();

    info!("SPI device{:?}", opt.cn0326_spi_device);
//    info!("I2C device{:?}", opt.cn0349_i2c_device);
    info!("MQTT IP {:?}", opt.mqtt_broker);

    m(&opt);
}

#[cfg(any(target_os = "linux", target_os = "android"))]
fn m(opt: &Opt) {
    let mqtt_options = MqttOptions::new(MQTT_CLIENT_ID, &opt.mqtt_broker, MQTT_SERVER_PORT);
    let (mut mqtt_client, _notifications) =
        MqttClient::start(mqtt_options).expect("Unable to connect to MQTT broker");

    let mut temperature = 0f64;
    let mut conductivity = 0f64;

    let spi: RefCell<Spidev> = RefCell::new(Spidev::open(&opt.cn0326_spi_device).unwrap());
    let options = SpidevOptions::new()
        .bits_per_word(8)
        .max_speed_hz(1000)
        .lsb_first(false)
        .mode(SpiModeFlags::SPI_MODE_3)
        .build();
    spi.borrow_mut().configure(&options).unwrap();

    let reset: [u8; 4] = [0xff; 4];
    let mut transfer1 = SpidevTransfer::write(&reset);
    transfer1.cs_change = 1;
    transfer1.delay_usecs = 1;
    spi.borrow_mut().transfer(&mut transfer1).unwrap();

    std::thread::sleep(std::time::Duration::from_millis(100));

    let bus = std::cell::RefCell::new(LinuxI2CBus::new(&opt.cn0349_i2c_device).unwrap());

    let cn0349_board = CN0349::new(bus);
    let cn0326_board = CN0326::new(spi);

    loop {
        let new_temperature = cn0349_board
            .measure_temperature()
            .expect("Unable to measure temperatue");

        let new_conductivity_uncompensated = 
            cn0349_board
            .measure_conductivity()
            .expect("Unable to measure conductivity");

        let new_conductivity = new_conductivity_uncompensated * (1.0 + ((ecF1 - new_temperature) * ecF2)) * ecF3;

        let new_uv = cn0326_board
            .measure_uv();

        let new_ph = cn0326_board
            .measure_ph(new_temperature);

        println!(
            "pH {:02}, uV:{:03}", new_ph, new_uv);

        if (new_temperature - temperature).abs() > PUBLISHING_HYSTERESIS_TEMPERATUR {
            temperature = new_temperature;
            mqtt_client
                .publish(
                    MQTT_TOPIC_TEMPERATURE,
                    QoS::AtLeastOnce,
                    true,
                    format!("{:.2}", new_temperature),
                )
                .expect("Error publishing temperature");
        }

        if (new_conductivity - conductivity).abs() > PUBLISHING_HYSTERESIS_CONDUCTIVITY {
            conductivity = new_conductivity;
            mqtt_client
                .publish(
                    MQTT_TOPIC_CONDUCTIVITY,
                    QoS::AtLeastOnce,
                    true,
                    format!("{:.3}", new_conductivity),
                )
                .expect("Error publishing conductivity");
        }

        if (new_ph - ph).abs() > PUBLISHING_HYSTERESIS_PH {
            ph = new_ph;
            mqtt_client
                .publish(
                    MQTT_TOPIC_PH,
                    QoS::AtLeastOnce,
                    true,
                    format!("{:.2}", new_ph),
                )
                .expect("Error publishing pH");
        }

        println!(
            "{{type: temp: {:.1}, cond_uncomp: {:.3}, cond: {:.3}}}",
            new_temperature, new_conductivity, new_conductivity_uncompensated
        );
    }
}

fn scan_all_buses() {
    const BusNums: [u8; 4] = [0, 1, 2, 4];

    let mut paths: Vec<String> = Vec::new();

    for i in &BusNums {
        let s = format!("/dev/i2c-{}", i);
        paths.push(s);
    }

    for i in paths {
        let mut bus = match LinuxI2CBus::new(&i) {
            Ok(bus) => bus,
            Err(_e) => {
                println!("Error opening I2C Bus {} {}", &i, _e);
                return;
            }
        };

        println!("Opened I2C Bus OK: {}", &i);

        let mut data = [0; 1];
        for x in 1..128 {
            let msg = LinuxI2CMessage::read(&mut data).with_address(x);

            match bus.transfer(&mut [msg]) {
                Ok(_rc) => println!("Found 7-bit addr 0x{:02X}", x),
                Err(_e) => {}
            }
        }
    }
}

fn read_nvidia_eeprom() {
    let mut eeprom_bus = match LinuxI2CBus::new("/dev/i2c-2") {
        Ok(bus) => bus,
        Err(_e) => {
            println!("Error opening EEPROM I2C Bus");
            return;
        }
    };

    let mut eeprom_data: [u8; 256] = [0; 256];
    let set_adr_command: [u8; 1] = [0x00];
    let set_adr = LinuxI2CMessage::write(&set_adr_command).with_address(0x57);
    let read_data = LinuxI2CMessage::read(&mut eeprom_data).with_address(0x57);

    match eeprom_bus.transfer(&mut [set_adr, read_data]) {
        Ok(_rc) => println!("Sucessfull read"),
        Err(_e) => {}
    }

    println!("{}", 1);
    for i in 0..256 {
        let ch: AsciiChar = match ascii::AsciiChar::from_ascii(eeprom_data[i]) {
            Ok(va) => va,
            Err(_) => AsciiChar::Dot,
        };

        print!("{}", ch); // , (eeprom_data[i] & 0x7f_u8));
        if (i + 1) % 16 == 0 {
            println!("");
        } else {
            print!(" ");
        }
    }
}
