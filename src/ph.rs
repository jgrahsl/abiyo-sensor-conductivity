

enum Reg {
    COMM_W = 0x0,
    STATUS_R = 0x0,
    MODE_16 = 0x1,
    CONF_16 = 0x2,
    DATA_24 = 0x3,
    ID = 0x4,
    IO = 0x5,
    OFFSET_24 = 0x6,
    FULL_24 = 0x7 
} 

extern crate spidev;
use std::io;
use std::io::prelude::*;
use spidev::{Spidev, SpidevOptions, SpidevTransfer, SPI_MODE_0};

fn create_spi() -> io::Result<Spidev> {
    let mut spi = try!(Spidev::open("/dev/spidev0.0"));
    let mut options = SpidevOptions::new()
         .bits_per_word(8)
         .max_speed_hz(20_000)
         .mode(SPI_MODE_0);
    try!(spi.configure(&options));
    Ok(spi)
}

/// perform half duplex operations using Read and Write traits
fn half_duplex(spi: &mut Spidev) -> io::Result<()> {
    let mut rx_buf = [0_u8; 10];
    try!(spi.write(&[0x01, 0x02, 0x03]));
    try!(spi.read(&mut rx_buf));
    println!("{:?}", rx_buf);
    Ok(())
}

/// Perform full duplex operations using Ioctl
fn full_duplex(spi: &mut Spidev) -> io::Result<()> {
    // "write" transfers are also reads at the same time with
    // the read having the same length as the write
    let mut transfer = SpidevTransfer::write(&[0x01, 0x02, 0x03]);
    try!(spi.transfer(&mut transfer));
    println!("{:?}", transfer.rx_buf);
    Ok(())
}

fn tttmain() {
    let mut spi = create_spi().unwrap();
    println!("{:?}", half_duplex(&mut spi).unwrap());
    println!("{:?}", full_duplex(&mut spi).unwrap());
}

/*

##########################################
##########################################


MODE_CONTINUOUS = 0x0
MODE_SINGLE = 0x1
MODE_IDLE = 0x2
MODE_POWERDOWN = 0x3
MODE_INTERNAL_ZERO = 0x4
MODE_INTERNAL_FULL = 0x5
MODE_SYSTEM_ZERO = 0x6
MODE_SYSTEM_FULL = 0x7

# 500ms conv cycle
UPDATE_RATE = 0xf
# Internal clock and output on CLK pin
CLOCK_REF = 0x0

# Internal or external reference
REF_SEL = 0
# Buffer adc input
BUFFER_INPUT = 0
# Gain settings
GAIN = 0
# boosting
BOOST = 0
# AIN1- is biased
BIAS = 0
# POLARITY
POLARITY = 0

### IO
IEXCDIR = 0x1
IEXCEN = 0x2

def comm_reg(rnw, reg):
    v = 0

    if (rnw):
        v = v | 0x40
    if ((reg >= 0) and (reg <= 7)):
        v = v | (reg << 3)
    return v
def status_is_err(reg):
    if (reg & 0x40):
        return True
    return False
def status_is_ready(reg):
    if (reg & 0x80):
        return False
    return True
def status_get_ch(reg):
    if (reg & 0x1):
        return 0x0
    if (reg & 0x2):
        return 0x1
    if (reg & 0x4):
        return 0x2
    return None


def read_reg(reg):
    read = spi.xfer([comm_reg(1,reg)],1)
    return (read[0])
def write_reg(reg, val):
    spi.write([comm_reg(0,reg), (val & 0xff)])
def read_16bit_reg(reg):
    read = spi.xfer([comm_reg(1,reg)],2)
    return ((read[0] << 8) | read[1])
def write_16bit_reg(reg, val):
    spi.write([comm_reg(0,reg), (val >> 8) & 0xff, (val & 0xff)])
def read_24bit_reg(reg):
    read = spi.xfer([comm_reg(1,reg)],3)
    return ((read[0] << 16) | read[1] << 8 | read[2])
def write_24bit_reg(reg, val):
    spi.write([comm_reg(0,reg), (val >> 16) & 0xff, (val >> 8) & 0xff, (val & 0xff)])



def check_id():
    read = read_reg(REG_ID)
    if ((read & 0xf) != 0xB):
        return False
    return True

def reset_device():
    spi.write([0xff,0xff,0xff,0xff])

def set_mode(mode):
    v = UPDATE_RATE | (CLOCK_REF << 6) | (mode << 13)
    write_16bit_reg(REG_MODE_16, UPDATE_RATE | (CLOCK_REF << 6) | (mode << 13))
    if (read_16bit_reg(REG_MODE_16) != v):
        return False
    return True

def set_conf(ch):
    v = ch | (BUFFER_INPUT << 4) | (REF_SEL << 7) | (GAIN << 8) | (BOOST << 11) | (POLARITY << 12) | (BIAS << 14)
    write_16bit_reg(REG_CONF_16, v)
    if (read_16bit_reg(REG_CONF_16) != v):
        return False
    return True

def set_io():
    v = (IEXCDIR << 2) | (IEXCEN << 0)
    write_reg(REG_IO, (IEXCDIR << 2) | (IEXCEN << 0))
    if (read_reg(REG_IO) != v):
        return False
    return True

spi.init()

reset_device()
if (not check_id()):
    print "Device not found"
    sys.exit(1)

if (not set_io()):
    print "Error setting IO_REG"
    sys.exit(1)

if (not set_conf(0)):
    print "Error setting CONF_REG"
    sys.exit(1)

if (not set_mode(MODE_IDLE)):
    print "Error setting CONF_MODE"
    sys.exit(1)

time.sleep(0.15)

set_mode(MODE_INTERNAL_ZERO)
while (True):
    if status_is_ready(read_reg(REG_STATUS_R)):
        break

set_conf(2)
set_mode(MODE_INTERNAL_FULL)
while (True):
    if status_is_ready(read_reg(REG_STATUS_R)):
        break

v = read_24bit_reg(REG_FULL_24)
set_conf(0)
write_24bit_reg(REG_FULL_24, v)

set_mode(MODE_SINGLE)

while (True):
    if status_is_ready(read_reg(REG_STATUS_R)):
        break
v = read_24bit_reg(REG_DATA_24)
uv = int(((v - 0x800000) / ((0x1000000-1)*1.0)) * 2100000.0)

j["uV"] = uv
j["pH"] = round((7000-(((uv - phF2) / phF1(temp)))*phF3)/1000,3)


print json.dumps(j)

sys.exit(0)
(END)
*/