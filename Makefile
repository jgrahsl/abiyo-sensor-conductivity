SHELL := /bin/bash
include .env

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help build shell run
.DEFAULT_GOAL := help

ARCH := $(shell arch)
IMAGE := $(CONTAINER)-$(ARCH)

pull:
	docker pull ${IMAGE}-base:${VERSION}
	docker pull ${IMAGE}-build:${VERSION}
	docker pull ${IMAGE}-test:${VERSION}
	docker pull ${IMAGE}-run:${VERSION}
	docker pull ${IMAGE}-dev:${VERSION}

shell:
	docker run -it --rm \
	-e CARGO_HOME=/src/.cargo \
	-v ${PWD}:/src \
	--workdir /src \
	--init \
	--entrypoint /bin/bash \
	--privileged \
	${IMAGE}-dev:${VERSION}

build:
	docker run -it --rm \
	-e CARGO_HOME=/src/.cargo \
	-v ${PWD}:/src \
	--workdir /src \
	--init \
	--entrypoint /usr/local/cargo/bin/cargo \
	--privileged \
	${IMAGE}-run:${VERSION} \
	build

run:
	docker run -it --rm \
	-e CARGO_HOME=/src/.cargo \
	-v ${PWD}:/src \
	--workdir /src \
	--init \
	--entrypoint /usr/local/cargo/bin/cargo \
	--privileged \
	${IMAGE}-run:${VERSION} \
	run

test:
	docker run -it --rm \
	-e CARGO_HOME=/src/.cargo \
	-v ${PWD}:/src \
	--workdir /src \
	--init \
	--entrypoint /usr/local/cargo/bin/cargo \
	--privileged \
	${IMAGE}-run:${VERSION} \
	test



install: build
	cp target/debug/abiyo-sensor-conductivity /usr/bin/
